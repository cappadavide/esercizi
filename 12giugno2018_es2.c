/*Sia L una lista doppiamente puntata e non circolre e T un ABR. Si implementi una
funzione che a. riempia con valori da tastiera le due strutture dati e ne stampi il contenuto.
b. Per ogni elemento della lista, si controlli se tale elemento è presente anche nell'albero e
in tal caso eliminarlo sia dalla lista che dall'albero. c. Stampare le due strutture dati al
termine dell'operazione precedente. */
//cappadavide


#include <stdio.h>
#include <stdlib.h>
#include "../../librerie_esame/BST/tree.h"

struct dlist{
    int info;
    struct dlist *next;
    struct dlist *prev;
};

typedef struct dlist* List;

List crealista(List L);
void stampalista(List L);
List insertincoda(List L,List P,int k);
Tree cancella(Tree T,int k,int *flag);
Tree cancellaradice(Tree T);
Tree staccamin(Tree T,Tree P);
List search_delete(List L,List P,Tree *T);


int main(){
    List L;
    Tree T;
    
    L=crealista(L);
    printf("La tua lista e' questa\n");
    stampalista(L);
    
    T=treeCreationMenu(0);
    printf("Preorder:\n");
    preOrderPrint(T);
    printf("Inorder:\n");
    inOrderPrint(T);
    
    L=search_delete(L,NULL,&T);

    printf("La tua lista e' questa\n");
    stampalista(L);
    printf("Preorder:\n");
    preOrderPrint(T);
    printf("Inorder:\n");
    inOrderPrint(T);

    return 0;
}
/*Funzioni liste */
List crealista(List L){
    int n,i;
    List tmp;
    printf("Quanti elementi vuoi inserire?\n");
    scanf("%d",&n);
    if(n!=0){
        L=(List)malloc(sizeof(struct dlist));
        printf("Inserisci elemento:\n");
        scanf("%d",&(L->info));
        L->prev=NULL;
        tmp=L;
        for(i=0;i<n-1;i++){
            tmp->next=(List)malloc(sizeof(struct dlist));
            printf("Inserisci elemento:\n");
            scanf("%d",&tmp->next->info);
            tmp->next->prev=tmp;
            tmp=tmp->next;
            
        }
        tmp->next=NULL;
    }
    else
        printf("Niente da fare\n");
    return L;
}

void stampalista(List L){
    while(L!=NULL){
        printf("%d->",L->info);
        L=L->next;
    }
    printf("NULL\n");
}

List insertincoda(List L,List P,int k){
    //printf("Sto qui\n");
    if(L!=NULL){
        L->next=insertincoda(L->next,L,k);
    }
    else{
        L=(List)malloc(sizeof(struct dlist));
        L->prev=P;
        L->next=NULL;
        L->info=k;
        
    }
    return L;
}
List search_delete(List L,List P,Tree *T){
    int flag=0;
    List tmp;
    if(L!=NULL){
        L->next=search_delete(L->next,L,T);
        *T=cancella(*T,L->info,&flag);
        if(flag==1){ //Allora nell'albero è stato trovato ed eliminato, eliminiamolo anche in lista
            if(P!=NULL){
                L->prev->next=L->next;
            }
            if(L->next!=NULL){
                L->next->prev=L->prev;
            }
            tmp=L;
            L=L->next;
            free(tmp);
        }
    }
    return L;
}

/////////////////////////////////////////////////////

Tree cancella(Tree T,int k,int *flag){
    if(T!=NULL){
        if(T->info<k){
            T->dx=cancella(T->dx,k,flag);
        }
        else if(T->info>k){
            T->sx=cancella(T->sx,k,flag);
        }   
        else{
            *flag=1;
            T=cancellaradice(T);
            
        }
    }
    return T;
}

Tree cancellaradice(Tree T){
    Tree tmp;
    if(T!=NULL){
        if(T->sx==NULL||T->dx==NULL){
           tmp=T;
            if(T->sx==NULL)
                T=T->dx;
            else
                T=T->sx;
        }
        else{
            tmp=staccamin(T->dx,T);
            T->info=tmp->info;
        }
        free(tmp);
    }
    return T;
}

Tree staccamin(Tree T,Tree P){
    if(T!=NULL){
        if(T->sx!=NULL)
            return staccamin(T->sx,T);
        else
        {
            if(T=P->sx)
                P->sx=T->dx;
            else
                P->dx=T->dx;
            return T;
            
        }
        
    }
    return NULL;
}
