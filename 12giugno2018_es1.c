/*Gioco di carte. Si consideri l'insieme delle carte napoletane, ovvero 40 carte numerate da
1 a 10 con 4 semi diversi (C,D,S,B). Si implementi;
a. una funzione che distribuisca in modo random 20+20 carte tra due stack Tom e Jerry,
più 20 valori booleani su uno stack Check, tutti implementatati con array.
b. una funzione gioco che, ad ogni turno, per livello, somma i valori di Tom e Jerry e
controlla la parità con Check. Se soddisfatta la parità, si rimuove la carta da Tom
altrimenti si rimuove da Jerry. Stampare il contenuto degli stack prima e dopo l'intero
gioco. Dire chi vince (lo stack con meno carte)*/

#include "../../librerie_esame/Stack/stack.h"
#include "../../librerie_esame/Input/inputReader.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
void distribuisci_carte(Stack S1,Stack S2,Stack Check);
void gioco(Stack S1,Stack S2,Stack Check);

int main(){
Stack Tom=initStack();
Stack Jerry=initStack();
Stack Check=initStack();
distribuisci_carte(Tom,Jerry,Check);
printf("Stack di Tom:\n");
printStack(Tom);
printf("\nStack di Jerry:\n");
printStack(Jerry);
gioco(Tom,Jerry,Check);
printStack(Tom);
printStack(Jerry);
if(dimStack(Tom)<dimStack(Jerry))
    printf("Ha vinto Tom!\n");
else if(dimStack(Tom)>dimStack(Jerry))
    printf("Ha vinto Jerry!\n");
else
    printf("Parita'! INCREDEBELESSEMO\n");
    

printf("\n");
system("pause");
return 0;
}

void distribuisci_carte(Stack S1,Stack S2,Stack Check){
srand((unsigned int)time(NULL));
int indice,n_carta;
int carte[40];
for(indice=0;indice<39;indice++)
    carte[indice]=0;
for(indice=0;indice<20;indice++){
    push(Check,rand()%2);
}
printf("Stack Check:\n");
printStack(Check);

while(dimStack(S1)!=20||dimStack(S2)!=20){
        n_carta=rand()%40;
        if(carte[n_carta]!=1&&dimStack(S1)!=20){
            //printf("Numero carta a Tom: %d\n",n_carta);
            carte[n_carta]=1;
            push(S1, n_carta);
        }
        n_carta=rand()%40;
        if(carte[n_carta]!=1&&dimStack(S2)!=20){
            //printf("Numero carta a Jerry: %d\n",n_carta);
            carte[n_carta]=1;
            push(S2,n_carta);
        }
}
}

void gioco(Stack S1,Stack S2,Stack Check){
    int val1,val2,boolean;
    if(!emptyStack(S1)&&!emptyStack(S2)){
        val1=pop(S1); val2=pop(S2); boolean=pop(Check);
        gioco(S1,S2,Check);
        //printf("Sommo %d e %d\n",val1%10+1,val2%10+1);
        if(((val1%10+1)+(val2%10+1))%2==boolean)
            push(S2,val2); //Elimina da Tom, quindi rimetti elemento solo in jerry
        else
            push(S1,val1); //Elimina da Jerry,quindi rimetti elemento solo in tom
        push(Check,boolean);
    }
}