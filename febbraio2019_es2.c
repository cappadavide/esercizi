/*Date due liste doppiamente linkate implementa la funzione di togli negativi che elimina da L1
tutti i negativi e i positivi da L2 senza cambiare ordine. Implementare la
funzione interleaving che date due liste restituisce il loro interleaving tale
che mette un elemento di L1 ogni due di L2 fino a che una delle due non si
annulla. */
//cappadavide

#include <stdio.h>
#include <stdlib.h>

struct dlist{
    int info;
    struct dlist *next;
    struct dlist *prev;
};

typedef struct dlist* List;

List crealista(List L);
void stampalista(List L);
List insertincoda(List L,List P,int k);
List toglinegativi(List L);
List toglipositivi(List L);
List interleaving_ITER(List L1,List L2);
List interleaving_RIC(List N,List L1,List L2,int count);


int main(){
List L1,L2,L3;
L1=crealista(L1);
printf("Fine prima lista\n");
L2=crealista(L2);
stampalista(L1);
stampalista(L2);
L1=toglinegativi(L1);
L2=toglipositivi(L2);
stampalista(L1);
stampalista(L2);
//L3=interleaving_ITER(L1,L2);
L3=interleaving_RIC(L3,L1,L2,0);
stampalista(L3);

return 0;
}
/*Funzioni liste doppiamente linkate non circolari */
List crealista(List L){
    int n,i;
    List tmp;
    printf("Quanti elementi vuoi inserire?\n");
    scanf("%d",&n);
    if(n>0){
        L=(List)malloc(sizeof(struct dlist));
        printf("Inserisci elemento:\n");
        scanf("%d",&(L->info));
        L->prev=NULL;
        tmp=L;
        for(i=0;i<n-1;i++){
            tmp->next=(List)malloc(sizeof(struct dlist));
            printf("Inserisci elemento:\n");
            scanf("%d",&tmp->next->info);
            tmp->next->prev=tmp;
            tmp=tmp->next;
            
        }
        tmp->next=NULL;
    }
    else
        printf("Niente da fare\n");
    return L;
}

void stampalista(List L){
    while(L!=NULL){
        printf("%d->",L->info);
        L=L->next;
    }
    printf("NULL\n");
}

List insertincoda(List L,List P,int k){
    //printf("Sto qui\n");
    if(L!=NULL){
        L->next=insertincoda(L->next,L,k);
    }
    else{
        L=(List)malloc(sizeof(struct dlist));
        L->prev=P;
        L->next=NULL;
        L->info=k;
        
    }
    return L;
}

////////////////////////////////////////////////
//Funzioni per la prova

List toglinegativi(List L){
    List tmp;
    if(L!=NULL){
        L->next=toglinegativi(L->next);
        if(L->info<0){
            tmp=L;
            if(L->prev!=NULL)
                L->prev->next=L->next;
            if(L->next!=NULL)
                L->next->prev=L->prev;
            L=L->next;
            free(tmp);
            

        }
    }
    return L;
}
List toglipositivi(List L){
    List tmp;
    if(L!=NULL){
        L->next=toglipositivi(L->next);
        if(L->info>=0){
            tmp=L;
            if(L->prev!=NULL)
                L->prev->next=L->next;
            if(L->next!=NULL)
                L->next->prev=L->prev;
            L=L->next;
            free(tmp);
            
        }
    }
    return L;
}
List interleaving_ITER(List L1,List L2){
    List L3=NULL;
    int count=0;
    //printf("Sto qui\n");
    while(L1!=NULL&&L2!=NULL){
        if(count==2){
            L3=insertincoda(L3,NULL,L1->info);
            L1=L1->next;
            count=0;
        }
        else{
            L3=insertincoda(L3,NULL,L2->info);
            L2=L2->next;
            count++;
        }
    }
    return L3;
}

List interleaving_RIC(List N,List L1,List L2,int count){ //Forse si può ottimizzare
    if(L1!=NULL&&L2!=NULL){
        if(count==2){
            N=insertincoda(N,NULL,L1->info);
            N=interleaving_RIC(N,L1->next,L2,0);
        }
        else{
            N=insertincoda(N,NULL,L2->info);
            N=interleaving_RIC(N,L1,L2->next,count+1);
        }
    }
    else{
        //printf("Sto qua\n");
        if(L1==NULL&&L2==NULL){
            return N;
        }
        if(L1==NULL){
            //printf("Solo L1 e' Null\n");
            N=insertincoda(N,NULL,L2->info);
            N=interleaving_RIC(N,L1,L2->next,0);
        }
         else if(L2==NULL){
            //printf("Solo L2 e' Null\n");
            N=insertincoda(N,NULL,L1->info);
            N=interleaving_RIC(N,L1->next,L2,0);
        }

    }
    return N;
}